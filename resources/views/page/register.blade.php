<!-- <html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>
    <form action="/login" method="POST">
        @csrf
        <input type="text" name="username" placeholder="username"><br>
        <input type="password" name="password" placeholder="password"><br>
        <br><br>
        <input type="submit" value="Sign in">
    </form>
</body>

</html> -->

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>
    <div class="content">
        <h1>Buat Account Baru!</h1>

        <div class="form">
            <h3>Sign Up Form</h3>
            
            <!--Name-->
            <form action="/welcome" method="POST">
                @csrf
                <label for="fname">First name : </label><br><br>
                <input type="text" id="fname" name="fname"><br><br>
                <label for="lname">Last name : </label><br><br>
                <input type="text" id="lname" name="lname"><br><br>

                <!--Radio button form Gender-->
                <label for="Gender">Gender</label><br><br>
                <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label><br>
                <input type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label><br>
                <input type="radio" id="other" name="gender" value="other">
                <label for="other">Other</label><br><br>

                <!--Nationality option-->
                <label for="nationality">Nationality</label><br><br>
                <select name="nationality" id="nationality">
                    <option value="Indonesian">Indonesian</option>
                    <option value="Singaporean">Singaporean</option>
                    <option value="Malaysian">Malaysian</option>
                    <option value="Others">Others</option>
                </select><br><br>

                <!--Languange Spoken-->
                <label for="language">Language Spoken : </label><br><br>
                <input type="checkbox" id="Bahasa" name="Bahasa" value="Bahasa">
                <label for="Bahasa">Bahasa Indonesia</label><br>
                <input type="checkbox" id="English" name="English" value="English">
                <label for="English">English</label><br>
                <input type="checkbox" id="Other" name="Other" value="Other">
                <label for="Other">Other</label><br><br>

                <!--Bio textarea-->
                <label for="bio">Bio</label><br><br>
                <textarea name="bio" id="bio" cols="30" rows="10"></textarea>

                <br><br>
                <input type="submit" value="Sign Up">
            </form>
        </div>
    </div>
</body>

</html>