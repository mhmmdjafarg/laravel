<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Page</title>
</head>

<body>
    <header>
        <h1>SanberBook</h1>
    </header>

    <div class="content">
        <div class="intro">
            <h2>Social Media Developer Santai Berkualitas</h2>
            <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
        </div>

        <div class="benefit">
            <h3>Benefit Join di SanberBook</h3>
            <ul>
                <li>Mendapatkan motivasi sesama developer</li>
                <li>Sharing knowledge dari para mastah Sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
        </div>

        <div class="join">
            <h3>Cara Bergabung ke SanberBook</h3>
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
        </div>
    </div>

</body>

</html>